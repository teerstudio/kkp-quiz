//
//  ProfileWorker.swift
//  KKP Quiz
//
//  Created by Thirawat Phannet on 28/12/2564 BE.
//  Copyright (c) 2564 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import Foundation
import Alamofire

class ProfileWorker {
    func getProfileData(completion: @escaping(ProfileModel) -> Void, failure: @escaping(ProfileError) -> Void) {
        guard let url = URL(string: ServiceUrl.getProfile.rawValue) else { return }
        AF.request(url).responseData { response in
            switch response.result {
            case .success(let data):
                do {
                    self.saveProfileDataToLocal(data: data)
                    if let localUserData = self.getProfileDataFromLocal() {
                        let jsonDecoder = JSONDecoder()
                        let jsonObject = try jsonDecoder.decode(ProfileModel.self, from: localUserData)
                        completion(jsonObject)
                    } else {
                        failure(ProfileError())
                    }
                } catch (let error) {
                    failure(ProfileError(description: error.localizedDescription))
                }
            case .failure(let error):
                failure(ProfileError(description: error.localizedDescription))
                break
            }
        }
    }
    
    func saveProfileDataToLocal(data: Data) {
        let encryptionData = Encryption.shared.encrypt(originalData: data)
        UserDefaults.standard.set(encryptionData, forKey: localKeyData.userData.rawValue)
    }
    
    func getProfileDataFromLocal() -> Data? {
        guard let localUserData = UserDefaults.standard.object(forKey: localKeyData.userData.rawValue) as? Data else { return nil }
        return Encryption.shared.decrypt(encryptedData: localUserData)
    }
}
