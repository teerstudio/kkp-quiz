//
//  ProfileInteractor.swift
//  KKP Quiz
//
//  Created by Thirawat Phannet on 28/12/2564 BE.
//  Copyright (c) 2564 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ProfileBusinessLogic {
    func getProfileData(request: Profile.getProfileData.Request)
}

protocol ProfileDataStore {
    
}

class ProfileInteractor: ProfileBusinessLogic, ProfileDataStore {
    var presenter: ProfilePresentationLogic?
    var worker: ProfileWorker = ProfileWorker()
    
    func getProfileData(request: Profile.getProfileData.Request) {
        worker.getProfileData(completion: { response in
            self.presenter?.presentGetProfileData(
                response: Profile.getProfileData.Response(
                    id: response.data.id,
                    email: response.data.email,
                    first_name: response.data.first_name,
                    last_name: response.data.last_name,
                    avatar: response.data.avatar
                )
            )
        }, failure: { error in
            self.presenter?.presentGetProfileDataError(response: Profile.getProfileData.Error(description: error.description))
        })
    }
}
