//
//  HomeItemCell.swift
//  KKP Quiz
//
//  Created by Thirawat Phannet on 29/12/2564 BE.
//

import UIKit
import SDWebImage

class HomeItemCell: UITableViewCell {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewImageBox: UIView!
    @IBOutlet weak var imageViewItem: UIImageView!
    @IBOutlet weak var labelItemName: UILabel!
    @IBOutlet weak var labelItemDescription: UILabel!
    @IBOutlet weak var viewDisplayColor: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBg.layer.masksToBounds = false
        viewBg.layer.shadowOffset = CGSize(width: 0, height: 3)
        viewBg.layer.shadowRadius = 3
        viewBg.layer.shadowOpacity = 0.3
        
        viewImageBox.layer.masksToBounds = false
        viewImageBox.layer.shadowOffset = CGSize(width: 0, height: 3)
        viewImageBox.layer.shadowRadius = 3
        viewImageBox.layer.shadowOpacity = 0.3
        
        viewImageBox.backgroundColor = .white
        contentView.backgroundColor = .white
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUpData(data: HomeQuickAccessItemDataModel) {
        labelItemName.text = data.item_name
        labelItemDescription.text = data.item_decription
        viewBg.backgroundColor = data.item_bg_color.hexStringToUIColor()
        viewDisplayColor.backgroundColor = data.item_color.hexStringToUIColor()
        
        checkColorLight(hexColor: data.item_bg_color)
        
        imageViewItem.sd_setImage(
            with: URL(string: data.item_img)
        )
    }
    
    private func checkColorLight(hexColor: String) {
        guard let isLight = hexColor.hexStringToUIColor().isLight() else { return }
        if isLight {
            labelItemName.textColor = .black
            labelItemDescription.textColor = .darkGray
        } else {
            labelItemName.textColor = .white
            labelItemDescription.textColor = .white
        }
    }
}
