//
//  ChartRouter.swift
//  KKP Quiz
//
//  Created by Thirawat Phannet on 28/12/2564 BE.
//  Copyright (c) 2564 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

@objc protocol ChartRoutingLogic {
    
}

protocol ChartDataPassing {
    var dataStore: ChartDataStore? { get }
}

class ChartRouter: NSObject, ChartRoutingLogic, ChartDataPassing {
    weak var viewController: ChartViewController?
    var dataStore: ChartDataStore?
    
}
