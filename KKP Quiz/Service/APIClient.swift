//
//  APIClient.swift
//  KKP Quiz
//
//  Created by Thirawat Phannet on 28/12/2564 BE.
//

import Alamofire

enum APIRouter: URLRequestConvertible {
    case get(url: String)
    case post(url: String)
    case put(url: String)
    
    private var url: String {
        switch self {
        case .get(let url):
            return url
        case .post(let url):
            return url
        case .put(let url):
            return url
        }
    }
    
    private var method: Alamofire.HTTPMethod {
        switch self {
        case .post:
            return .post
        case .get:
            return .get
        case .put:
            return .put
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try url.asURL()
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return urlRequest
    }
}



class APIClient {
    @discardableResult
    static func request<T:Decodable>(route: APIRouter, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T, AFError>)->Void) -> DataRequest {
        return  AF.request(route).responseDecodable (decoder: decoder){ (response: DataResponse<T, AFError>) in
//            guard let request = response.request else { return }
//            #if DEBUG
//            NetworkLogger.log(request: request)
//            switch response.result {
//            case .success:
//                NetworkLogger.log(response:response.response, data: response, error: nil)
//            case .failure(let error):
//                NetworkLogger.log(response:response.response, data: response, error: error)
//            }
//            #endif
            completion(response.result)
         }
    }
}
