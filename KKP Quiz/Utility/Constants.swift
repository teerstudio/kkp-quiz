//
//  Constants.swift
//  KKP Quiz
//
//  Created by Thirawat Phannet on 28/12/2564 BE.
//

import Foundation

enum Secure: String {
    case encryptionKey = "!&@(*)*)*(&HY*G(!@"
}

enum ServiceUrl: String {
    case getProfile = "https://reqres.in/api/users/2"
    case getBanners = "https://reqres.in/api/banner..."
}

enum localKeyData: String {
    case userData = "USER_DATA"
}

enum localFileName: String {
    case homePage = "home_page.json"
    case goldYearly = "Gold_Yearly.json"
}
