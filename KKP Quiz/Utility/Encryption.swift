//
//  Encryption.swift
//  KKP Quiz
//
//  Created by Thirawat Phannet on 28/12/2564 BE.
//

import Foundation
import RNCryptor

struct Encryption {
    static let shared = Encryption()
    private let key: String = Secure.encryptionKey.rawValue
    func encrypt(originalText: String) -> Data? {
        if let data: Data = String(originalText).data(using: .utf8) {
            return RNCryptor.encrypt(data: data, withPassword: key)
        } else {
            return nil
        }
    }
    
    func encrypt(originalData: Data) -> Data {
        return RNCryptor.encrypt(data: originalData, withPassword: key)
    }
    
    func decrypt(encryptedData: Data) -> String? {
        do {
            let originalData = try RNCryptor.decrypt(data: encryptedData, withPassword: key)
            return String(data: originalData, encoding: .utf8) ?? ""
        } catch {
            return nil
        }
    }
    
    func decrypt(encryptedData: Data) -> Data? {
        do {
            let originalData = try RNCryptor.decrypt(data: encryptedData, withPassword: key)
            return originalData
        } catch {
            return nil
        }
    }
}
