//
//  LocalFile.swift
//  KKP Quiz
//
//  Created by Thirawat Phannet on 28/12/2564 BE.
//

import Foundation

struct LocalFile {
    static let shared = LocalFile()
    
    func readJsonChartFile(completion: @escaping([ChartItemModel]?) -> Void) {
        do {
            let jsonURL = Bundle.main.url(forResource: localFileName.goldYearly.rawValue.fileName(), withExtension: "json")
            let jsonDecoder = JSONDecoder()
            let jsonData = try Data(contentsOf: jsonURL!)
            let arrayObject = try jsonDecoder.decode([ChartItemModel].self, from: jsonData)
            completion(arrayObject)
        } catch {
            completion(nil)
        }
    }
    
    func readJsonHomePageFile(completion: @escaping(HomeDataModel?) -> Void) {
        do {
            let jsonURL = Bundle.main.url(forResource: localFileName.homePage.rawValue.fileName(), withExtension: "json")
            let jsonDecoder = JSONDecoder()
            let jsonData = try Data(contentsOf: jsonURL!)
            let dataObject = try jsonDecoder.decode(HomeDataModel.self, from: jsonData)
            completion(dataObject)
        } catch {
            completion(nil)
        }
    }
}
